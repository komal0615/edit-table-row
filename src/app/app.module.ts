import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { EditRowComponent } from './edit-row/edit-row.component';
import {TableModule} from "primeng/table";
import {DropdownModule} from "primeng/dropdown";
import {FormsModule} from "@angular/forms";
import {ButtonModule, TabViewModule} from "primeng/primeng";
import {EditService} from "./edit-row/edit.service";
import {HttpClientModule} from "@angular/common/http";
import {AddRowDirective} from "./edit-row/add-row.directive";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  declarations: [
    AppComponent,
    EditRowComponent,
      AddRowDirective
  ],
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        TabViewModule,
        TableModule,
        DropdownModule,
        FormsModule,
        HttpClientModule,
        ButtonModule


    ],
  providers: [EditService],
  bootstrap: [AppComponent]
})
export class AppModule { }
