import {Directive, Input, HostListener} from '@angular/core';
import {Table} from 'primeng/table';

@Directive({
    selector: '[pAddRow]'
})
export class AddRowDirective {
    @Input() table: Table;
    @Input() newRow: any;

    @HostListener('click', ['$event'])
    onClick(event: Event) {
        console.log('event', event);
        console.log('Table data 1---',this.table);
        // Insert a new row
        this.table.value.push(this.newRow);
        console.log('value pushed', this.newRow)
        console.log('Table data---',this.table);
        // Set the new row in edit mode
        this.table.initRowEdit(this.newRow);
        event.preventDefault();
    }
}
