import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


export interface Car {
  id:string;
  year: string;
  JAN: string;
  FEB: string;
  MARCH: string;
  APRIL: string;
  MAY: string;
  JUNE: string;
  JULY:string;
  AUG:string;
  SEPT: string;
  OCT: string;
  NOV: string;
  DEC: string;
}
@Injectable({
  providedIn: 'root'
})
export class EditService {

  constructor(private http: HttpClient) { }

  getCars(): Car[] {
    return this.data;
  }

  data: Car[] =  [
    {id:'1', year: "2001", JAN: "89.83", FEB: "89.83", MARCH: "89.83",APRIL: "89.83",MAY: "89.83",JUNE:"89.83",JULY:"89.83",AUG:"89.83", SEPT:"89.83", OCT:"89.83",NOV: "89", DEC:"89.83" },
    {id:'2', year: "2001", JAN: "89.83", FEB: "89.83", MARCH: "89.83",APRIL: "89.83",MAY: "89.83",JUNE:"89.83",JULY:"89.83",AUG:"89.83", SEPT:"89.83", OCT:"89.83",NOV: "89", DEC:"89.83" },
    {id:'3', year: "2001", JAN: "89.83", FEB: "89.83", MARCH: "89.83",APRIL: "89.83",MAY: "89.83",JUNE:"89.83",JULY:"89.83",AUG:"89.83", SEPT:"89.83", OCT:"89.83",NOV: "89", DEC:"89.83" },
    {id:'4', year: "2001", JAN: "89.83", FEB: "89.83", MARCH: "89.83",APRIL: "89.83",MAY: "89.83",JUNE:"89.83",JULY:"89.83",AUG:"89.83", SEPT:"89.83", OCT:"89.83",NOV: "89", DEC:"89.83" },
    {id:'5', year: "2001", JAN: "89.83", FEB: "89.83", MARCH: "89.83",APRIL: "89.83",MAY: "89.83",JUNE:"89.83",JULY:"89.83",AUG:"89.83", SEPT:"89.83", OCT:"89.83",NOV: "89", DEC:"89.83" },
    {id:'6', year: "2001", JAN: "89.83", FEB: "89.83", MARCH: "89.83",APRIL: "89.83",MAY: "89.83",JUNE:"89.83",JULY:"89.83",AUG:"89.83", SEPT:"89.83", OCT:"89.83",NOV: "89", DEC:"89.83" },
    {id:'7', year: "2001", JAN: "89.83", FEB: "89.83", MARCH: "89.83",APRIL: "89.83",MAY: "89.83",JUNE:"89.83",JULY:"89.83",AUG:"89.83", SEPT:"89.83", OCT:"89.83",NOV: "89", DEC:"89.83" },

  ]
}
