import {Component, OnInit, Input} from "@angular/core";
import {SelectItem} from 'primeng/api';

@Component({
    selector: 'app-edit-row',
    templateUrl: './edit-row.component.html',
    styleUrls: ['./edit-row.component.css']
})
export class EditRowComponent implements OnInit {
    tableData: Array<tableDataFormat>;
    tableDataCUF: Array<tableDataCUFFormat>;
    cols: any[];
    clonedRecord: { [s: string]: tableDataFormat } = {};
    clonedRecord1: { [s: string]: tableDataCUFFormat } = {};
    years: SelectItem[];

    ngOnInit(): void {
        this.years = [
            {label: '2001', value: '2001'},
            {label: '2002', value: '2002'},
            {label: '2003', value: '2003'},
            {label: '2004', value: '2004'},
            {label: '2005', value: '2005'},
            {label: '2006', value: '2006'},
            {label: '2007', value: '2007'}, {label: '2008', value: '2008'},
            {label: '2009', value: '2009'}, {label: '2010', value: '2010'},
            {label: '2011', value: '2011'}, {label: '2012', value: '2012'},

        ];
        this.cols = [
            {field: 'id', header: 'ID'},
            {field: 'year', header: 'Year'},
            {field: 'jan', header: 'JAN'},
            {field: 'feb', header: 'FEB'},
            {field: 'march', header: 'MARCH'},
            {field: 'april', header: 'APRIL'},
            {field: 'may', header: 'MAY'},
            {field: 'june', header: 'JUNE'},
            {field: 'july', header: 'JULY'},
            {field: 'aug', header: 'AUG'},
            {field: 'sept', header: 'SEPT'},
            {field: 'oct', header: 'OCT'},
            {field: 'nov', header: 'NOV'},
            {field: 'dec', header: 'DEC'},
        ];
        this.tableData = [
            {
                reportID: '0',
                year: "2001",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                reportID: '1',
                year: "2002",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                reportID: '2',
                year: "2003",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                reportID: '3',
                year: "2004",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                reportID: '4',
                year: "2005",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                reportID: '5',
                year: "2006",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                reportID: '6',
                year: "2007",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                reportID: '7',
                year: "2008",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
        ];
        this.tableDataCUF = [
            {
                id: '0',
                year: "2001",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                id: '1',
                year: "2002",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                id: '2',
                year: "2003",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                id: '3',
                year: "2004",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                id: '4',
                year: "2005",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                id: '5',
                year: "2006",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                id: '6',
                year: "2007",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
            {
                id: '7',
                year: "2008",
                JAN: "89.83",
                FEB: "89.83",
                MARCH: "89.83",
                APRIL: "89.83",
                MAY: "89.83",
                JUNE: "89.83",
                JULY: "89.83",
                AUG: "89.83",
                SEPT: "89.83",
                OCT: "89.83",
                NOV: "89",
                DEC: "89.83"
            },
        ];

        this.getTableData();

    }

    onRowEditInit(data: tableDataFormat) {
        // console.log('colned----', data);
        this.clonedRecord[data.reportID] = {...data};
    }

    onRowEditSave(data: tableDataFormat) {
        // console.log('save---', data);
        delete this.clonedRecord[data.reportID];
    }

    onRowEditCancel(data: tableDataFormat,index:number) {
        // console.log('cancel---', data, index);
        this.tableData[index] = this.clonedRecord[data.reportID];
        delete this.clonedRecord[data.reportID];
    }

    onRowDelete(data){
       // console.log('delete index----',data);
       // var deleterow = this.tableData.indexOf(data);
       this.tableData.splice(data,1);
       // console.log('delete row---',deleterow);
    }

    onRowEditInit1(data: tableDataCUFFormat) {
        // console.log('colned----', data);
        this.clonedRecord1[data.id] = {...data};
    }

    onRowEditSave1(data: tableDataCUFFormat) {
        // console.log('save---', data);
        delete this.clonedRecord1[data.id];
    }

    onRowEditCancel1(data: tableDataCUFFormat, index: number) {
        // console.log('cancel---', data, index);
        this.tableDataCUF[index] = this.clonedRecord1[data.id];
        delete this.clonedRecord1[data.id];
    }
    onRowDelete1(data){
        // console.log('delete index----',data);
        // var deleterow = this.tableData.indexOf(data);
        this.tableData.splice(data,1);
        // console.log('delete row---',deleterow);
    }

    getTableData() {
        console.log('table data called');
    }

    newRow() {
        return {
            year: '',
            JAN: '',
            FEB: '',
            MARCH: '',
            APRIL: '',
            MAY: '',
            JUNE: '',
            JULY: '',
            AUG: '',
            SEPT: '',
            OCT: '',
            NOV: '',
            DEC: '',
            reportID: ''

        };
    }

}

export interface tableDataFormat {
    reportID: string;
    year: string;
    JAN: string;
    FEB: string;
    MARCH: string;
    APRIL: string;
    MAY: string;
    JUNE: string;
    JULY: string;
    AUG: string;
    SEPT: string;
    OCT: string;
    NOV: string;
    DEC: string;
}

export interface tableDataCUFFormat {
    id: string;
    year: string;
    JAN: string;
    FEB: string;
    MARCH: string;
    APRIL: string;
    MAY: string;
    JUNE: string;
    JULY: string;
    AUG: string;
    SEPT: string;
    OCT: string;
    NOV: string;
    DEC: string;
}
